from django.shortcuts import render, redirect
from django.http import Http404
from .models import Musicvideo, Category, Tag
from django.core.paginator import Paginator
import re

# Create your views here.
def top(request):
    return render(request,'top.html')
class viewsVideo:
    def list(request):
        #typeナシならリストを表示
        #関数を分けるとredirectが必要だったが最後のreturnが引っ掛かる
        #typeには、create, detial, update, deleteを実装
        #create以外にはmusicvideo識別用にmusicIDパラメータを使う
        video_list = Musicvideo.objects.filter(trash = False).exclude(videoURL = "").order_by('date')
        sort = ''
        order = ''
        page = 1
        if "sort" in request.GET:
            if request.GET['sort'] == 'date':
                sort = 'date'
                if request.GET['order'] == 'down':
                    video_list = Musicvideo.objects.filter(trash = False).exclude(videoURL = "").order_by('date')
                    order = 'down'
                if request.GET['order'] == 'up':
                    video_list = Musicvideo.objects.filter(trash = False).exclude(videoURL = "").order_by('date').reverse()
                    order = 'up'
            elif request.GET['sort'] == 'title':
                sort = 'title'
                if request.GET['order'] == 'down':
                    order = 'down'
                    video_list = Musicvideo.objects.filter(trash = False).exclude(videoURL = "").order_by('title')
                elif request.GET['order'] == 'up':
                    order = 'up'
                    video_list = Musicvideo.objects.filter(trash = False).exclude(videoURL = "").order_by('title').reverse()
        if "page" in request.GET:
            page = int(request.GET['page'])
        video_pages = Paginator(video_list,20)
        video_page = video_pages.page(page)
        #リンク用の数字リスト作成
        max_page = video_pages.num_pages
        if max_page <= 5:
            page_nums = list(range(1, max_page+1))
        else:
            if page <= 3:
                page_nums = list(range(1,6))
            elif not ((max_page - page) <= 2) or page <= 2:
                page_nums = list(range(page-2,page+3))
            elif max_page - page <= 2:
                page_nums = list(range(max_page-4,max_page+1))
        content={
            'videolist':video_page,
            'sort':sort,
            'order':order,
            'page_nums':page_nums,
            'max_page':max_page,
            'page':page,
            }
        return render(request,'video/list.html',content)

    def register(request):
        if request.method == 'POST':
            video = Musicvideo.objects.create(
                    title = request.POST['title'],
                    description = request.POST['description'],
                    contributor = request.POST['contributor'],
                    videoURL = request.POST['videoURL'],
                    originalVideoURL = request.POST['originalvideoURL']
                    )
            if request.POST['category']:
                video.category = Category.objects.get(pk = request.POST['category'])
                video.save()
            #入力されたカンマで区切られた文字列を
            # finditerと正規表現をつかってtag_listに抽出
            matches = re.finditer(r'(\w|\s)+',request.POST['tag'])
            tag_list = []
            for match in matches:
                tag_list.append(match.group())
            #taggitを使ってvideo.tagに追加
            for tag in tag_list:
                video.tag.add(tag)
                #自作タグクラスに名前に対応したインスタンスがなければ新規生成
                if not Tag.objects.filter(name = tag):
                    Tag.objects.create(name = tag)
            return redirect("video")
        category_list = Category.objects.all()
        content = {'category_list':category_list}
        return render(request, 'video/register.html', content)

    def detail(request,video_id):
        video = Musicvideo.objects.get(pk=video_id)
        if video.tag.names():
            tag_list = video.tag.names()
        else:
            tag_list = []
        len_tag_list = len(tag_list)
        content = {
            'video':video,
            'tag_list':tag_list,
            'len_tag':len_tag_list
            }
        return render(request, 'video/detail.html', content)

    def update(request,video_id):
        editvideo = Musicvideo.objects.get(pk=video_id)
        if request.method == 'POST':
            editvideo.title = request.POST['title']
            editvideo.description = request.POST['description']
            editvideo.contributor = request.POST['contributor']
            editvideo.category = Category.objects.get(pk = int(request.POST['category']))
            editvideo.videoURL = request.POST['videoURL']
            editvideo.originalVideoURL = request.POST['originalvideoURL']
            editvideo.save()
            #video.tagを一旦全消去(リセット)
            editvideo.tag.clear()
            #入力されたカンマで区切られた文字列を
            # finditerと正規表現をつかってtag_listに抽出
            matches = re.finditer(r'(\w)+',request.POST['tag'])
            tag_list = []
            for match in matches:
                tag_list.append(match.group())
            #taggitを使ってvideo.tagに追加
            for tag in tag_list:
                editvideo.tag.add(tag)
                #自作タグクラスに名前に対応したインスタンスがなければ新規生成
                if not Tag.objects.filter(name = tag):
                    Tag.objects.create(name = tag)
            return redirect("video")
        #taggitlistにtaggitに紐づいたタグを取得
        taggitlist = editvideo.tag.names()
        tags = ""
        #取得したリストからタグの名前を文字列に追加していく
        for tag in taggitlist:
            tags += tag + ","
        #文末の余計な,を削除
        tags = tags[:len(tags)-1]
        category_list = Category.objects.all()
        content = {
            'category_list':category_list,
            'video':editvideo,
            "tag":tags
            }
        return render(request, 'video/update.html', content)

    def delete(request,video_id):
        video=Musicvideo.objects.get(pk=video_id)
        video.delete()
        return redirect("video")

    def copy(request, video_id):
        sourcevideo = Musicvideo.objects.get(pk=video_id)
        if request.method == 'POST':
            video = Musicvideo.objects.create(
                    title = request.POST['title'],
                    description = request.POST['description'],
                    contributor = request.POST['contributor'],
                    videoURL = request.POST['videoURL'],
                    originalVideoURL = request.POST['originalvideoURL']
                    )
            if request.POST['category']:
                video.category = Category.objects.get(pk = request.POST['category'])
                video.save()
            #入力されたカンマで区切られた文字列を
            # finditerと正規表現をつかってtag_listに抽出
            matches = re.finditer(r'(\w|\s)+',request.POST['tag'])
            tag_list = []
            for match in matches:
                tag_list.append(match.group())
            #taggitを使ってvideo.tagに追加
            for tag in tag_list:
                video.tag.add(tag)
                #自作タグクラスに名前に対応したインスタンスがなければ新規生成
                if not Tag.objects.filter(name = tag):
                    Tag.objects.create(name = tag)
            return redirect("video")
        #taggitlistにtaggitに紐づいたタグを取得
        taggitlist = sourcevideo.tag.names()
        tags = ""
        #取得したリストからタグの名前を文字列に追加していく
        for tag in taggitlist:
            tags += tag + ","
        #文末の余計な,を削除
        tags = tags[:len(tags)-1]
        category_list = Category.objects.all()
        content = {
            'category_list':category_list,
            'video':sourcevideo,
            "tag":tags
            }
        return render(request, 'video/update.html', content)

    def addtrash(request, video_id):
        video = Musicvideo.objects.get(pk=video_id)
        video.trash = True
        video.save()
        return redirect("video")

    def trash(request):
        video_list=Musicvideo.objects.filter(trash = True)
        content={'videolist':video_list}
        return render(request,'video/trash.html',content)

    def videoaddtag(request):
        if request.method == 'POST':
            add_video_id_list = request.POST.getlist('video')
        video_list=Musicvideo.objects.all()

        page =  4
        max_page = 11
        if max_page <= 5:
            page_nums = list(range(1, max_page+1))
        else:
            if page <= 3:
                page_nums = list(range(1,6))
            elif not ((max_page - page) <= 2) or page <= 2:
                page_nums = list(range(page-2,page+3))
            elif max_page - page <= 2:
                page_nums = list(range(max_page-4,max_page+1))
        
        content = {
            'videolist':video_list,
            'page_nums': page_nums,
            'page':page,
            'max_page':max_page,
            }
        return render(request,'pager.html',content)

    def nolink(request):
        video_list = Musicvideo.objects.filter(trash = False).order_by('date')
        sort = ''
        order = ''
        page = 1
        if "sort" in request.GET:
            if request.GET['sort'] == 'date':
                sort = 'date'
                if request.GET['order'] == 'down':
                    video_list = Musicvideo.objects.filter(trash = False).order_by('date')
                    order = 'down'
                if request.GET['order'] == 'up':
                    video_list = Musicvideo.objects.filter(trash = False).order_by('date').reverse()
                    order = 'up'
            elif request.GET['sort'] == 'title':
                sort = 'title'
                if request.GET['order'] == 'down':
                    order = 'down'
                    video_list = Musicvideo.objects.filter(trash = False).order_by('title')
                elif request.GET['order'] == 'up':
                    order = 'up'
                    video_list = Musicvideo.objects.filter(trash = False).order_by('title').reverse()
        if "page" in request.GET:
            page = int(request.GET['page'])
        video_pages = Paginator(video_list,20)
        video_page = video_pages.page(page)
        #リンク用の数字リスト作成
        max_page = video_pages.num_pages
        if max_page <= 5:
            page_nums = list(range(1, max_page+1))
        else:
            if page <= 3:
                page_nums = list(range(1,6))
            elif not ((max_page - page) <= 2) or page <= 2:
                page_nums = list(range(page-2,page+3))
            elif max_page - page <= 2:
                page_nums = list(range(max_page-4,max_page+1))
        content={
            'videolist':video_page,
            'sort':sort,
            'order':order,
            'page_nums':page_nums,
            'max_page':max_page,
            'page':page,
            }
        return render(request,'video/list.html',content)


class viewsTag:
    def list(request):
        if 'type' in request.GET:
            if request.GET['type'] == 'non_edited':
                nonedited_tag_list = Tag.objects.filter(edited=False)
                content = {
                    'nonedited_tag_list':nonedited_tag_list
                    }
                return render(request,'tag/noneditedlist.html', content)
        tag_list = Tag.objects.all()
        nonedited_tag_list = Tag.objects.filter(edited = False)[:4]
        content = {
            'taglist':tag_list,
            'nonedited_tag_list':nonedited_tag_list
            }
        return render(request, 'tag/list.html', content)

    def detail(request,tag_name):
        #tag付けされてる動画を何件か出せたりしたら
        # (一覧表示を引っ張ってくるのは自作クラスの方で)
        # (関連動画を引っ張って来るのはtaggitの方で)
        # 全動画を確認の場合は詳細ページの方でタグによる検索ページにとばす
        tag = Tag.objects.get(name = tag_name)
        taged_video = Musicvideo.objects.filter(tag__name__in = [tag_name])
        taged_number = len(taged_video)
        if taged_number > 4:
            taged_video = taged_video[:3]
        content = {
            'tag':tag,
            'taged_video':taged_video,
            'taged_number':taged_number
            }
        return render(request, 'tag/detail.html', content)

    def update(request, tag_name):
        tag = Tag.objects.get(name = tag_name)
        if request.method == 'POST':
            if request.POST['name']:
                tag.name = request.POST['name']
            tag.description = request.POST['description']
            tag.descriptURL = request.POST['descriptURL']
            tag.edited = True
            tag.save()
            return redirect("tagdetail",tag.name)
        content = {'tag':tag}
        return render(request,'tag/update.html',content)

    def delete(request,tag_name):
        tag = Tag.objects.get(name = tag_name)
        tag.delete()
        return redirect("tag")

class viewsCategory:
    def list(request):
        category_list=Category.objects.all()
        for category in category_list:
            category.videonum = len(Musicvideo.objects.filter(category__name = category.name))
        content = {"category_list":category_list}
        return render(request, "category/list.html", content)
    
    def create(request):
        if request.method == 'POST':
            category = Category.objects.create(name = request.POST['name'])
            return redirect('category')
        return render(request, 'category/create.html')
    
    def update(request, category_id):
        editcategory = Category.objects.get(pk=category_id)
        if request.method == 'POST':
            editcategory.name = request.POST['name']
            editcategory.save()
            return redirect('category')
        content = {'editcategory':editcategory}
        return render(request, 'category/update.html', content)

    def delete(request, category_id):
        editcategory = Category.objects.get(pk = category_id)
        editcategory.delete()
        return redirect('category')

class Search:
    def search(request):
        return render(request,'search.html')

"""
class viewsSite:
    def list(request):
        site_list=Site.objects.all()
        content={'site_list':site_list}
        return render(request,'site/list.html',content)
    
    def create(request):
        if request.method == 'POST':
            if request.POST['name'] and request.POST['baseURL']:
                site=Site.objects.create(
                        name=request.POST['name'],
                        baseURL=request.POST['baseURL']
                        )
            return redirect('site')
        return render(request, 'site/create.html')

    def update(request, site_id):
        editsite=Site.objects.get(pk=site_id)
        if request.method == 'POST':
            editsite.name = request.POST['name']
            editsite.baseURL = request.POST['baseURL']
            editsite.save()
            return redirect('site')
        content = {'editsite':editsite}
        return render(request, 'site/update.html', content)
    
    def delete(request, site_id):
        editsite=site.objects.get(pk=site_id)
        editsite.delete()
        return redirect('site')
"""