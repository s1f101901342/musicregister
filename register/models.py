from django.db import models
from django.utils import timezone
import datetime
from taggit.managers import TaggableManager

# Create your models here.
class Category(models.Model):
    #カテゴリー分け用(ボカロ歌ってみた演奏してみたなどなど)
    name=models.CharField(max_length=31,unique=True)
    
    def __str__(self):
        return self.name

"""
class Site(models.Model):
    #ニコ動とかyoutubeとかの分類用
    name=models.CharField(max_length=31)
    baseURL=models.SlugField()
    
    def __str__(self):
        return self.name

class URL(models.Model):
    urlbody=models.SlugField()
    site=models.ForeignKey(Site, related_name='site', on_delete=models.DO_NOTHING)

    def makeURL(urlbody, site_id):
        url=URL.objects.create(
            urlbody=urlbody,
            site=Site.objects.get(pk=site_id)
        )
        return url
    
    def updateURL(urlbody, site_id, url_id):
        url=URL.objects.get(pk=url_id)
        url.urlbody=urlbody
        url.site=Site.objects.get(pk=site_id)
        url.save()
    
    def __str__(self):
        return self.urlbody
"""

class Tag(models.Model):
    #外部キーとしての利用ではなくtaggitで作成したタグへの
    # 解説やらのURLやらの追加のためのクラス
    name=models.TextField(unique=True)
    edited=models.BooleanField(default=False)
    description=models.TextField(max_length=255)
    #ニコニコ大百科とか公式サイトのURLでも
    descriptURL=models.TextField(max_length=255)
    
    def __str__(self):
        return self.name
    

class Musicvideo(models.Model):
    title=models.CharField(max_length=47)
    trash=models.BooleanField(default=False)
    description=models.TextField(max_length=255)
    date=models.DateTimeField(default=timezone.now)
    contributor=models.CharField(max_length=31)
    #投稿者、ボカロＰ、歌い手、演奏者、などなど
    category=models.ForeignKey(Category, related_name='videocategory', on_delete=models.DO_NOTHING, blank=True, null=True)
    #tag管理はtaggit
    tag=TaggableManager(blank=True)
    videoURL=models.TextField(max_length=255, blank=True, null=True)    
    #原曲リンク用のカラム
    originalVideoURL=models.TextField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.title