from django.urls import path
from . import views

urlpatterns = [
    path('', views.top, name='top'),
    path('search', views.Search.search, name='search'),
    #ここからビデオ
    path('video', views.viewsVideo.list, name='video'),
    path('video/register', views.viewsVideo.register, name='videoregister'),
    path('video/<int:video_id>/detail', views.viewsVideo.detail, name='videodetail'),
    path('video/<int:video_id>/update', views.viewsVideo.update, name='videoupdate'),
    path('video/<int:video_id>/delete', views.viewsVideo.delete, name='videodelete'),
    path('video/<int:video_id>/copy', views.viewsVideo.copy, name='videocopy'),
    path('video/<int:video_id>/trash', views.viewsVideo.addtrash, name='videoaddtrash'),
    path('video/trash', views.viewsVideo.trash, name='videotrash'),
    path('video/nolink', views.viewsVideo.nolink, name='videonolink'),
    path('video/addtag', views.viewsVideo.videoaddtag, name='videoaddtag'),
    #ここからタグ
    path('tag', views.viewsTag.list, name='tag'),
    path('tag/name=<str:tag_name>', views.viewsTag.detail, name='tagdetail'),
    path('tag/delete/name=<str:tag_name>', views.viewsTag.delete, name='tagdelete'),
    path('tag/update/name=<str:tag_name>', views.viewsTag.update, name='tagupdate'),
    #ここからカテゴリー
    path('category', views.viewsCategory.list, name='category'),
    path('category/create', views.viewsCategory.create, name='categorycreate'),
    path('category/update/<int:category_id>', views.viewsCategory.update, name='categoryupdate'),
    path('category/delete/<int:category_id>', views.viewsCategory.delete, name='categorydelete'),
]
"""
#ここからサイト
path('site', views.viewsSite.list, name='site'),
path('site/create', views.viewsSite.create, name='sitecreate'),
path('site/update/<int:site_id>', views.viewsSite.update, name='siteupdate'),
path('site/delete/<int:site_id>', views.viewsSite.delete, name='sitedelete'),
"""