from django import template
import re

register = template.Library()

@register.filter
def addstr(arg1, arg2):
    return str(arg1) + str(arg2)

@register.filter
def for_update_page(url):
    #page=数字 を削除
    url = re.sub("page=\d+","",url)
    #?&,&&など被った場合各々1つに置換
    url = re.sub("\?&","?",url)
    url = re.sub("&&","&",url)
    #文末に残った邪魔な?,&を削除
    url = re.sub("&$","",url)
    url = re.sub("\?$","",url)
    return url

#本来やりたかった方
"""
if re.search("\?page=\d+$",url):#パラメータがpageのみ
    url = re.sub("\?page=\d+","",url)
elif re.search("&page=\d+",url):
    rul = re.sub("&page=\d+","",url)
else:
    url = re.sub("page=\d+&","",url)
"""